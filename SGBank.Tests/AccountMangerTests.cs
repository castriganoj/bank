﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SGBank.BLL;
using SGBank.Data;
using SGBank.Models;

namespace SGBank.Tests
{
    [TestFixture]
    public class AccountMangerTests
    {
        [Test]
        public void FoundAccountReturnsSuccess()
        {
            var manager = new AccountManager();

            var response = manager.GetAccount(1);

            Assert.IsTrue(response.Success);
            Assert.AreEqual(1, response.Data.AccountNumber);
            Assert.AreEqual("Mary", response.Data.FirstName);
        }

        [Test]
        public void NotFoundAccountReturnsFail()
        {
            var manager = new AccountManager();

            var response = manager.GetAccount(9999);

            Assert.IsFalse(response.Success);
        }

        [Test]
        public void WithDrawTest()
        {
            var manager = new AccountManager();
            var acct = new AccountRepository();
           var accTtest = acct.LoadAccount(1);

            var response = manager.WithDraw(accTtest, 10.00M);

            Assert.IsTrue(response.Success);
        }

        [Test]
        public void TransferTest()
        {
            var manager = new AccountManager();
            var acct = new AccountRepository();
            var accTtest = acct.LoadAccount(1);
            var balance = accTtest.Balance;
            var accTtest2 = acct.LoadAccount(2);

            var response = manager.Transfer(accTtest, 2, 10.00M);

            Assert.IsTrue(response.Data.NewBalance == balance-10.00M);
        }
        [Test]
        public void CreateAccountTest()
        {
            var repo = new AccountRepository();
            var manager = new AccountManager();
            manager.CreateAccount("Ronald", "McDonald", 200.00M);

            var accounts = repo.GetAllAccounts();

            var testAccount = accounts.Exists(p => p.LastName == "McDonald" && p.FirstName == "Ronald");

            Assert.AreEqual(testAccount, true);

        }

        [Test]
        public void DeleteAccountTest()
        {
            var repo = new AccountRepository();
            var manager = new AccountManager();
            manager.CreateAccount("LeBron", "James",200.00M);
            manager.DeleteAccount(5);

            var accounts = repo.GetAllAccounts();

            var testAccount = accounts.Exists(p => p.AccountNumber==5);

            Assert.AreEqual(testAccount, false);

        }

    }
}
