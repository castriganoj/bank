﻿using System;
using SGBank.BLL;
using SGBank.Models;

namespace SGBank.UI.Workflows
{
    public class TransferWorkflow
    {
        public void Execute(Account account)
        {

            int accountto = GetAccountNumber();
            decimal amount = GetTransferAmount();

            var manager = new AccountManager();

            var response = manager.Transfer(account,accountto, amount);

            if (response.Success)
            {
                Console.Clear();
                Console.WriteLine("Transfer {0:c} to account {1}, from account {2}.  New Balance is {3}.", response.Data.TransferAmount, response.Data.AccountNumberTo, response.Data.AccountNumber, response.Data.NewBalance);
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("An error occurred.  {0}", response.Message);
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }
        }

        private decimal GetTransferAmount()
        {
            do
            {
                Console.Write("Enter a transfer amount: ");
                var input = Console.ReadLine();
                decimal amount;

                if (decimal.TryParse(input, out amount))
                    return amount;

                Console.WriteLine("That was not a valid amount. Please try again.");
                Console.ReadKey();
            } while (true);

        }

        private int GetAccountNumber()
        {
            do
            {
                Console.Write("Enter Account Number to transfer to: ");
                var input = Console.ReadLine();
                int amount;

                if (int.TryParse(input, out amount))
                    return amount;

                Console.WriteLine("That was not a valid number. Please try again.");
                Console.ReadKey();
            } while (true);

        }
    }
}