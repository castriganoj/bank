﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.BLL;

namespace SGBank.UI.Workflows
{
    class CreateAccountWorkflow
    {
        public void Execute()
        {

            var firstName = GetFirstName();
            var lastName = GetLastName();
            var initialDeposit = GetInitialDeposit();

            var manager = new AccountManager();
            var response = new AccountManager().CreateAccount(firstName, lastName, initialDeposit);

            if (response.Success)
            {
                Console.Clear();
                Console.WriteLine(String.Format("Created an account for {0},{1}. Initial deposit was ${2}",lastName,firstName,initialDeposit));
                Console.ReadKey();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("An error occurred.  {0}", response.Message);
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }


        }


        private string GetFirstName()
        {
            do
            {
                Console.Write("Enter a first name: ");
                var input = Console.ReadLine();
                string FirstName = input;

                if (FirstName.Length > 0)
                    return FirstName;

                Console.WriteLine("That was not a valid name. Please try again.");
                Console.ReadKey();
            } while (true);
        }

        private string GetLastName()
        {
            do
            {
                Console.Write("Enter a last name: ");
                var input = Console.ReadLine();
                string LastName = input;

                if (LastName.Length > 0)
                    return LastName;

                Console.WriteLine("That was not a valid last name. Please try again.");
                Console.ReadKey();
            } while (true);
        }

        private decimal GetInitialDeposit()
        {
            do
            {
                Console.Write("Enter an initial deposit amount: ");
                var input = Console.ReadLine();
                decimal amount;

                if (decimal.TryParse(input, out amount))
                    return amount;

                Console.WriteLine("That was not a valid amount. Please try again.");
                Console.ReadKey();
            } while (true);
        }

    }
}
