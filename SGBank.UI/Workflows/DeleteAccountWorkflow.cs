﻿using System;
using SGBank.BLL;

namespace SGBank.UI.Workflows
{
    public class DeleteAccountWorkflow
    {
        public void Execute()
        {
            var accountNumberToDelete = GetAccountNumberFromUser();

            var manager = new AccountManager();
            var response = manager.DeleteAccount(accountNumberToDelete);

            if (response.Success)
            {
                Console.Clear();
                Console.WriteLine("Deleted account number {0} which belonged to {1}, {2}.\nIt had a balance of ${3}.",response.Data.AccountNumber,response.Data.LastName,response.Data.FirstName,response.Data.BalanceDeleted);
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }
            else
            {
                Console.Clear();
                Console.WriteLine("An error occurred.  {0}", response.Message);
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }

        }

        public int GetAccountNumberFromUser()
        {
            do
            {
                Console.Clear();
                Console.WriteLine("Enter an account number for the account you want to delete: ");
                string input = Console.ReadLine();
                int accountNumber;

                if (int.TryParse(input, out accountNumber))
                    return accountNumber;

                Console.WriteLine("That was not a valid account number.  Press any key to try again...");
                Console.ReadKey();
            } while (true);

        }
    }
}