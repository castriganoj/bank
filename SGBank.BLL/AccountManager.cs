﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SGBank.Data;
using SGBank.Models;

namespace SGBank.BLL
{
    public class AccountManager
    {
        public Response<Account> GetAccount(int accountNumber)
        {
            var repo = new AccountRepository();
            var response = new Response<Account>();

            try
            {
                var account = repo.LoadAccount(accountNumber);

                if (account == null)
                {
                    response.Success = false;
                    response.Message = "Account was not found!";
                }
                else
                {
                    response.Success = true;
                    response.Data = account;
                }
            }
            catch (Exception ex)
            {
                // log the exception
                response.Success = false;
                response.Message = "There was an error.  Please try again later.";
            }

            return response;
        }

        public Response<DepositReciept> CreateAccount(string firstName, string lastName, decimal initialDeposit)
        {

            var response = new Response<DepositReciept>();
            try
            {
                if (initialDeposit <= 0)
                {
                    response.Success = false;
                    response.Message = "Must deposit a positive value.";
                }
                else
                {
                    var repo = new AccountRepository();

                    List<Account> accounts = repo.GetAllAccounts();
                    var highAccountNum = accounts.Select(a => a.AccountNumber).Max();

                    var newAccount = new Account();

                    newAccount.FirstName = firstName;
                    newAccount.LastName = lastName;
                    newAccount.Balance = initialDeposit;
                    newAccount.AccountNumber = highAccountNum + 1;
                    accounts.Add(newAccount);
                    repo.OverwriteFileWithNewAccount(accounts);

                    response.Success = true;
                    response.Data = new DepositReciept();
                    response.Data.AccountNumber = newAccount.AccountNumber;
                    response.Data.DepositAmount = initialDeposit;
                    response.Data.NewBalance = newAccount.Balance;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;


        }

        public Response<TransferReciept> Transfer(Account account, int account2, decimal amount)
        {
            var acct = new AccountRepository();
            var accTto = acct.LoadAccount(account2);

            var response = new Response<TransferReciept>();

            try
            {
                if (amount <= 0)
                {
                    response.Success = false;
                    response.Message = "Must transfer a positive value.";
                }
                else
                {
                    account.Balance -= amount;
                    accTto.Balance += amount;
                    var repo = new AccountRepository();
                    repo.UpdateAccount(account);
                    repo.UpdateAccount(accTto);
                    response.Success = true;

                    response.Data = new TransferReciept();
                    response.Data.AccountNumber = account.AccountNumber;
                    response.Data.AccountNumberTo = accTto.AccountNumber;

                    response.Data.TransferAmount = amount;
                    response.Data.NewBalance = account.Balance;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;

            }

            return response;

        }

        public Response<WithDrawReciept> WithDraw(Account account, decimal amount)
        {
            var response = new Response<WithDrawReciept>();



            try
                {
                    if (amount <= 0)
                    {
                        response.Success = false;
                        response.Message = "Must deposit a positive value.";
                    }
                    else
                    {
                        account.Balance -= amount;
                        var repo = new AccountRepository();
                        repo.UpdateAccount(account);
                        response.Success = true;

                        response.Data = new WithDrawReciept();
                        response.Data.AccountNumber = account.AccountNumber;
                        response.Data.WithDrawAmount = amount;
                        response.Data.NewBalance = account.Balance;
                    }
                }
                catch (Exception ex)
                {
                    response.Success = false;
                    response.Message = ex.Message;

                }

                return response;

        }

        public Response<DepositReciept> Deposit(Account account, decimal amount)
        {
            
            var response = new Response<DepositReciept>();

            try
            {
                if (amount <= 0)
                {
                    response.Success = false;
                    response.Message = "Must deposit a positive value.";
                }
                else
                {
                    account.Balance += amount;
                    var repo = new AccountRepository();
                    repo.UpdateAccount(account);
                    response.Success = true;

                    response.Data = new DepositReciept();
                    response.Data.AccountNumber = account.AccountNumber;
                    response.Data.DepositAmount = amount;
                    response.Data.NewBalance = account.Balance;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public Response<DeleteAccountReciept> DeleteAccount(int accountNumberToDelete)
        {

            var response = new Response<DeleteAccountReciept>();
            try
            {
                if (accountNumberToDelete <= 0)
                {
                    response.Success = false;
                    response.Message = "Must deposit a positive value.";
                }
                else
                {
                    var repo = new AccountRepository();

                    List<Account> accounts = repo.GetAllAccounts();
                    Account deleted = accounts.Single(p=>p.AccountNumber == accountNumberToDelete);
                    accounts.Remove(deleted);
                    
            
                    repo.OverwriteFileWithNewAccount(accounts);

                    response.Success = true;
                    response.Data = new DeleteAccountReciept();
                    response.Data.AccountNumber = deleted.AccountNumber;
                    response.Data.FirstName = deleted.FirstName;
                    response.Data.LastName = deleted.LastName;
                    response.Data.BalanceDeleted = deleted.Balance;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;




        }
    }
}
